from django.db import models


class Pengumuman (models.Model):
    judul = models.CharField(max_length = 100 , null=True)
    detail = models.CharField(max_length = 1000,null=True)
    tanggal = models.DateTimeField(auto_now=True)
    penting = models.BooleanField(max_length = 10,null=False, default=False)
    
    def __str__(self):
        return self.judul
