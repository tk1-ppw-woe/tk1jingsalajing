from django.http import HttpResponse
from django.shortcuts import render,redirect
from .models import Pengumuman
from . import forms

def tambah_pengumuman(request):
    form = forms.Input_Form()
    if (request.method == "POST"):
        form = forms.Input_Form(request.POST)
        if form.is_valid():
            form.save()
            return redirect('pengumuman:lihat_pengumuman')
    response = {
        'form' : form
    }
    return render(request,'tambah_pengumuman.html',response)

def lihat_pengumuman(request):
    list_pengumuman = Pengumuman.objects.all()
    response = {
        'list' : list_pengumuman
    }
    return render(request,'pengumuman.html',response)


def hapus_pengumuman(request,id):
    Pengumuman.objects.get(id=id).delete()
    return redirect('pengumuman:lihat_pengumuman')







