from django import forms
from . import models

class Input_Form(forms.ModelForm):
    class Meta:
        model = models.Pengumuman
        fields = "__all__"

    judul = forms.CharField(
        widget= forms.TextInput(
            attrs= {
                "class" : "form_pengumuman",
                "max_length" : 100,
                "required"  : True,
                "placeholder"   : "judul"
            
            }
        )
    )

    detail = forms.CharField(
        widget = forms.Textarea(
            attrs = {
                "class" : "form_pengumuman",
                "max_length" : 10000000,
                "required"  : True,
                "placeholder" : "detail",
            }
        )
    )
