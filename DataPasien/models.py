from django.db import models

# Create your models here.
class Pasien(models.Model):
    nama = models.CharField(max_length=100)
    umur = models.PositiveIntegerField(default=0)
    alamat = models.TextField()
    status = models.CharField('Status Covid',max_length=30,choices=(('Positif','Positif'),('Negatif','Negatif')))
    def __str__(self):
        return self.nama.title()

class Sembuh(models.Model):
    jml = models.IntegerField()
    

class Meninggal(models.Model):
    jml = models.IntegerField()
