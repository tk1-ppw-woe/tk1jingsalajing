from django.test import TestCase, Client
from django.urls import reverse, resolve
from DataPasien.forms import Patient
from .models import Pasien, Sembuh, Meninggal
from .views import listofpasien, counter, add, edit, hapus, tambahmeninggal, tambahsembuh
# Create your tests here.
class DataPasienTestCase(TestCase):

    def test_url_listofpasien(self):
        response = Client().get('/listofpasien/')
        self.assertEqual(response.status_code, 200)

    def test_url_add(self):
        response = Client().get('/add/')
        self.assertEqual(response.status_code, 200)

    def test_url_counter(self):
        response = Client().get('/counter/')
        self.assertEqual(response.status_code, 200)

    def test_url_edit(self):
        test = Pasien.objects.create(nama= "Honda", umur=7, alamat="Suzuki", status="Positif")
        response = Client().get('/edit/{id}/'.format(id=str(test.id)))
        self.assertEqual(response.status_code, 200)

    def test_url_hapus(self):
        test = Pasien.objects.create(nama= "Honda", umur=7, alamat="Suzuki", status="Positif")
        response = Client().get('/delete/{id}/'.format(id=str(test.id)))
        self.assertEqual(response.status_code, 200)

    def test_url_sembuh(self):
        test = Pasien.objects.create(nama= "Honda", umur=7, alamat="Suzuki", status="Positif")
        response = Client().get('/sembuh/{id}/'.format(id=str(test.id)))
        self.assertEqual(response.status_code, 200)

    def test_url_meninggal(self):
        test = Pasien.objects.create(nama= "Honda", umur=7, alamat="Suzuki", status="Positif")
        response = Client().get('/meninggal/{id}/'.format(id=str(test.id)))
        self.assertEqual(response.status_code, 200)

    def test_TemplateUsed_listofpasien(self):
        response = Client().get('/listofpasien/')
        self.assertTemplateUsed(response, 'DataPasien/listofpasien.html')

    def test_TemplateUsed_add(self):
        response = Client().get('/add/')
        self.assertTemplateUsed(response, 'DataPasien/add.html')

    def test_TemplateUsed_counter(self):
        response = Client().get('/counter/')
        self.assertTemplateUsed(response, 'DataPasien/counter.html')

    def test_TemplateUsed_edit(self):
        test = Pasien.objects.create(nama= "Honda", umur=7, alamat="Suzuki", status="Positif")
        response = Client().get('/edit/{}/'.format(str(test.id)))
        self.assertTemplateUsed(response, 'DataPasien/edit.html')

    def test_TemplateUsed_hapus(self):
        test = Pasien.objects.create(nama= "Honda", umur=7, alamat="Suzuki", status="Positif")
        response = Client().get('/delete/{}/'.format(str(test.id)))
        self.assertTemplateUsed(response, 'DataPasien/hapus.html')

    def test_TemplateUsed_sembuh(self):
        test = Pasien.objects.create(nama= "Honda", umur=7, alamat="Suzuki", status="Positif")
        response = Client().get('/sembuh/{}/'.format(str(test.id)))
        self.assertTemplateUsed(response, 'DataPasien/sembuh.html')

    def test_TemplateUsed_meninggal(self):
        test = Pasien.objects.create(nama= "Honda", umur=7, alamat="Suzuki", status="Positif")
        response = Client().get('/meninggal/{}/'.format(str(test.id)))
        self.assertTemplateUsed(response, 'DataPasien/meninggal.html')

    def create_pasien(self):
        return Pasien.objects.create(nama= "", umur=7, alamat="", status="")

    def test_creation_pasien(self):
        test = self.create_pasien()
        self.assertTrue(isinstance(test,Pasien))
        self.assertEqual(test.__str__(), test.nama)
        
    def create_sembuh(self):
        return Sembuh.objects.create(jml=7)

    def test_creation_sembuh(self):
        test = self.create_sembuh()
        self.assertTrue(isinstance(test,Sembuh))

    def create_meninggal(self):
        return Meninggal.objects.create(jml=7)

    def test_creation_meninggal(self):
        test = self.create_meninggal()
        self.assertTrue(isinstance(test,Meninggal))

    def test_valid_form(self):
        test = Pasien.objects.create(nama= "Honda", umur=7, alamat="Suzuki", status="Positif")
        data = {'nama': test.nama, 'umur': test.umur, 'alamat': test.alamat, 'status': test.status,}
        form = Patient(data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        test = Pasien.objects.create(nama= "", umur=0, alamat="", status="")
        data = {'nama': test.nama, 'umur': test.umur, 'alamat': test.alamat, 'status': test.status,}
        form = Patient(data=data)
        self.assertFalse(form.is_valid())

    def test_url_is_resolved_listofpasien(self):
        url = reverse('DataPasien:listofpasien')
        self.assertEqual(resolve(url).func, listofpasien)

    def test_url_is_resolved_counter(self):
        url = reverse('DataPasien:counter')
        self.assertEqual(resolve(url).func, counter)

    def test_url_is_resolved_add(self):
        url = reverse('DataPasien:add')
        self.assertEqual(resolve(url).func, add)

    def test_url_is_resolved_edit(self):
        test = Pasien.objects.create(nama= "Honda", umur=7, alamat="Suzuki", status="Positif")
        url = reverse('DataPasien:update', kwargs={'pk':str(test.id)})
        self.assertEqual(resolve(url).func, edit)   

    def test_url_is_resolved_hapus(self):
        test = Pasien.objects.create(nama= "Honda", umur=7, alamat="Suzuki", status="Positif")
        url = reverse('DataPasien:delete', kwargs={'pk':str(test.id)})
        self.assertEqual(resolve(url).func, hapus)
        
    def test_url_is_resolved_meninggal(self):
        test = Pasien.objects.create(nama= "Honda", umur=7, alamat="Suzuki", status="Positif")
        url = reverse('DataPasien:meninggal', kwargs={'pk':str(test.id)})
        self.assertEqual(resolve(url).func, tambahmeninggal)

    def test_url_is_resolved_sembuh(self):
        test = Pasien.objects.create(nama= "Honda", umur=7, alamat="Suzuki", status="Positif")
        url = reverse('DataPasien:sembuh', kwargs={'pk':str(test.id)})
        self.assertEqual(resolve(url).func, tambahsembuh)
    
