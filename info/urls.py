from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

app_name = 'info'

urlpatterns = [
    path('formpegawai/', views.halaman_input, name = 'form' ),
    path('info/',views.buka_halaman_info, name = 'info'),
    path('ubahinfo/',views.halaman_info),
    path('formhapus/',views.halaman_hapus),
    path('hapus/',views.hapus),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)