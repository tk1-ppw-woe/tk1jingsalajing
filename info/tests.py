from django.test import TestCase,Client
from django.urls import resolve
from info.views import halaman_input,buka_halaman_info,halaman_hapus
from info.models import Profile

# Create your tests here.
class TestInfo(TestCase):
    def test_url_formpegawai(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_info(self):
        response = Client().get('/info')
        self.assertEqual(response.status_code, 301)
    
    def test_url_hapus(self):
        response = Client().get('/formhapus')
        self.assertEqual(response.status_code, 301)
    
    def test_template_formpegawai(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'base.html')
    
    def test_formpegawai_success(self):
        test = 'protes'
        response_post = Client().post('/', {'nama': test, 'alamat':test, 'no_telp':test})
        self.assertEqual(response_post.status_code, 200) 


