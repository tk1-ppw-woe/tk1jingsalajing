from django.urls import path

from .views import forum,addpost,komen,addkomen, hapuskomen, hapuspost

app_name = 'forum'

urlpatterns = [
    path('forum', forum, name='forum'),
    path('addpost', addpost, name='addpost'),
    path('comment/<int:id>',komen,name='comment'),
    path('addcomment/<int:id>',addkomen,name='addkomen'),
    path('hapuskomen/<int:id>',hapuskomen,name='hapuskomen'),
    path('hapuspost/<int:id>',hapuspost,name='hapuspost'),
]