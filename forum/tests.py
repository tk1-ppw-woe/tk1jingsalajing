from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import forum,addpost, komen,addkomen
from .forms import Form_Post, Form_Comment
from .models import Post, Comment

# Create your tests here.
class TestForum(TestCase):
    def test_url_forum(self):
        response = Client().get('/forum')
        self.assertEqual(response.status_code, 200)

    def test_url_add_post(self):
        response = Client().get('/addpost')
        self.assertEqual(response.status_code, 200)

    def test_template_forum(self):
        response = Client().get('/forum')
        self.assertTemplateUsed(response, 'forum.html')

    def test_template_addpost(self):
        response = Client().get('/addpost')
        self.assertTemplateUsed(response, 'add.html')
    
    def test_func_forum(self):
        found = resolve('/forum')
        self.assertEqual(found.func, forum)

    def test_func_addpost(self):
        found = resolve('/addpost')
        self.assertEqual(found.func, addpost)
  
    def test_addpost_success_and_show(self):
        test = 'protes'
        response_post = Client().post('/addpost', {'nama': test, 'judul':test, 'pesan':test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/forum')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_addpost_failed(self):
        test = 'protes'
        response_post = Client().post('/addpost', {'nama': '','judul':'','pesan':''})
        self.assertEqual(response_post.status_code, 302)
    
        response= Client().get('/forum')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_url_show_post_and_comment(self):
        test = 'protes'
        post = Post.objects.create(nama= test, judul= test, pesan=test)
        response = Client().get('/comment/' + str(post.id) )
        self.assertEqual(response.status_code, 200)

    def test_url_add_comment(self):
        test = 'protes'
        post = Post.objects.create(nama= test, judul= test, pesan=test)
        response = Client().get('/addcomment/' + str(post.id) )
        self.assertEqual(response.status_code, 200)

    def test_func_show_post_and_comment(self):
        test = 'protes'
        post = Post.objects.create(nama= test, judul= test, pesan=test)
        found = resolve('/comment/' + str(post.id))
        self.assertEqual(found.func, komen)

    def test_func_add_comment(self):
        test = 'protes'
        post = Post.objects.create(nama= test,judul= test, pesan=test)
        found = resolve('/addcomment/' + str(post.id))
        self.assertEqual(found.func, addkomen)

    def test_template_show_post_and_comment(self):
        test = 'protes'
        post = Post.objects.create(nama= test, judul= test, pesan=test)
        response = Client().get('/comment/' + str(post.id) )
        self.assertTemplateUsed(response, 'detail.html')

    def test_template_add_comment(self):
        test = 'protes'
        post = Post.objects.create(nama= test,judul= test, pesan=test)
        response = Client().get('/addcomment/' + str(post.id) )
        self.assertTemplateUsed(response, 'add.html')

    def test_addcomment_success_and_show(self):
        test = 'protes'
        post = Post.objects.create(nama= 'abc', judul= 'abc', pesan='abc')
        response_post = Client().post('/addcomment/'+str(post.id), {'nama': test,'komentar':test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/comment/'+str(post.id))
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_addcomment_failed(self):
        test = 'protes'
        post = Post.objects.create(nama = 'abc', judul='abc', pesan='abc')
        response_post = Client().post('/addcomment/'+str(post.id), {'nama': '','komentar':''})
        self.assertEqual(response_post.status_code, 302)
    
        response= Client().get('/comment/'+str(post.id))
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_delete_post_success(self):
        test = 'protes'
        post = Post.objects.create(nama = test, judul=test, pesan=test)
        response= Client().get('/hapuspost/'+str(post.id))
        jumlah = Post.objects.all().count()
        self.assertEqual(0,jumlah)

    def test_delete_comment_success(self):
        test = 'protes'
        post = Post.objects.create(nama = test, judul=test, pesan=test)
        komen= Comment.objects.create(post=post, nama=test, komentar="test")
        response= Client().get('/hapuskomen/'+str(komen.id))
        jumlah = Comment.objects.all().count()
        self.assertEqual(0,jumlah)


  





